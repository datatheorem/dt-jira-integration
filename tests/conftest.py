from unittest import mock

import pytest
from datatheorem_api_client import JiraIntegrationConfig
from datatheorem_api_client import ResultsAPIClient
from datatheorem_api_client.results_api.jira_integration_config import SeverityFieldConfig

from dt_jira_integration.client import JipyApiClient


@pytest.fixture(scope="session")
def config() -> JiraIntegrationConfig:
    jira_config = JiraIntegrationConfig(
        id=9,  # GLOBAL ID
        is_global=True,
        base_url="https://google.atlassian.net",
        username="username",
        password="password",
        type_of_issue_name="1",
        project_key_or_id="PROJ",
        export_filter="ALL_ISSUES",
        export_pre_prod=True,
        export_prod=True,
        severity_field_config=SeverityFieldConfig("customfield_1", "2", "3", "4"),
        static_fields=[],
        dynamic_fields=[],
    )
    return jira_config


@pytest.fixture()
def client(config: JiraIntegrationConfig) -> JipyApiClient:
    client = mock.Mock(spec=JipyApiClient)
    client._jira = mock.Mock()
    return client(config)


@pytest.fixture()
def results_client() -> ResultsAPIClient:
    return mock.Mock(spec=ResultsAPIClient)
