from unittest import mock
from unittest.mock import call

import pytest
from datatheorem_api_client import JiraIntegrationConfig
from datatheorem_api_client import SecurityFinding
from datatheorem_api_client import SecurityFindingTarget

from dt_jira_integration.client import JipyApiClient
from dt_jira_integration.sync_with_jira_action import FindingTargetJiraSyncAction
from dt_jira_integration.sync_with_jira_action import FindingTargetToJiraFormatter
from dt_jira_integration.utils import JiraCommentTrackingTagEnum

from .factories import MobileAppFactory


@pytest.mark.parametrize("status", ["NEW", "OPEN"])
def test_finding_target_is_valid_and_not_saved_in_jira(
    status: str, config: JiraIntegrationConfig, client: JipyApiClient
) -> None:
    # Given a finding with a status of <status>
    finding = SecurityFinding(
        1,
        "title",
        "HIGH",
        "DATA_AT_REST_EXPOSURE",
        "EASY",
        "description",
        "recommendation",
        1,
        None,
        None,
        "portal_url",
        "OPEN",
        secure_code="code here",
    )
    finding_target = SecurityFindingTarget(1, "text", status, "date", 2, "finding_id", finding.portal_url, None, None)
    formatter = FindingTargetToJiraFormatter(config)

    mobile_app = MobileAppFactory(name="name", platform="ANDROID")
    jira_sync_action = FindingTargetJiraSyncAction(finding_target, finding, mobile_app, formatter, client)

    # When finding is going through the syncing process
    jira_sync_action.perform()

    # Then the finding should of been saved to Jira
    assert client._jira.create_issue.call_count == 1


@pytest.mark.parametrize(
    "status",
    [
        "CLOSED_COMPENSATING_CONTROL",
        "CLOSED_FIXED",
        "CLOSED_ITEM_NOT_FOUND",
        "CLOSED_RISK_ACCEPTED",
        "OPEN_READY_TO_RESCAN",
    ],
)
def test_finding_target_is_NOT_valid_and_not_saved_in_jira(
    status: str, config: JiraIntegrationConfig, client: JipyApiClient
) -> None:
    # Given a finding with a status of <status>
    finding = SecurityFinding(
        1,
        "title",
        "HIGH",
        "P1",
        "exploitability",
        "description",
        "recommendation",
        1,
        None,
        None,
        "portal_url",
        "HIGH",
        "code here",
    )
    finding_target = SecurityFindingTarget(1, "text", status, "date", 2, "finding_id", finding.portal_url, None, None)
    formatter = FindingTargetToJiraFormatter(config)
    mobile_app = mock.Mock()

    # When finding is going through the syncing process
    jira_sync_action = FindingTargetJiraSyncAction(finding_target, finding, mobile_app, formatter, client)
    jira_sync_action.perform()

    # Then the finding should NOT of been saved to Jira
    assert client._jira.create_issue.call_count == 0


@pytest.mark.parametrize("status", ["CLOSED_FIXED", "CLOSED_ITEM_NOT_FOUND"])
def test_finding_target_has_external_id_and_has_no_messages_and_has_been_fixed(
    status: str, config: JiraIntegrationConfig, client: mock.Mock
) -> None:
    # Given a finding with a status of <status> that has been fixed
    finding = SecurityFinding(
        1,
        "title",
        "HIGH",
        "P1",
        "exploitability",
        "description",
        "recommendation",
        1,
        None,
        None,
        "portal_url",
        "HIGH",
        "code here",
    )
    finding_target = SecurityFindingTarget(
        1,
        "text",
        status,
        "date",
        2,
        "finding_id",
        finding.portal_url,
        None,
        None,
        external_id="external_id",
    )
    formatter = FindingTargetToJiraFormatter(config)
    mobile_app = mock.Mock()

    # When finding goes through the syncing process
    jira_sync_action = FindingTargetJiraSyncAction(finding_target, finding, mobile_app, formatter, client)

    # And finding has no comment
    with mock.patch(
        "dt_jira_integration.sync_with_jira_action.get_comments_from_issue",
        return_value=[],
    ):
        jira_sync_action.perform()

    # Then DT posts a comment on Jira saying it's fixed
    reason = JiraCommentTrackingTagEnum.TARGET_FIXED.value
    msg = f"{reason} - Data Theorem detected that this issue has been addressed in the latest scan."

    client.add_comment.assert_called_once_with(client.get_issue(), msg)


@pytest.mark.parametrize("status", ["CLOSED_FIXED", "CLOSED_ITEM_NOT_FOUND"])
def test_finding_target_has_external_id_and_has_ignore_messages_and_has_been_fixed(
    status: str, config: JiraIntegrationConfig, client: mock.Mock
) -> None:
    # Given a finding with status of <status> that has been fixed
    finding = SecurityFinding(
        1,
        "title",
        "HIGH",
        "P1",
        "exploitability",
        "description",
        "recommendation",
        1,
        None,
        None,
        "portal_url",
        "HIGH",
        "code here",
    )
    finding_target = SecurityFindingTarget(
        1,
        "text",
        status,
        "date",
        2,
        "finding_id",
        finding.portal_url,
        None,
        None,
        external_id="external_id",
    )
    formatter = FindingTargetToJiraFormatter(config)

    # When finding goes through the syncing process
    mobile_app = MobileAppFactory(name="name", platform="platform")
    jira_sync_action = FindingTargetJiraSyncAction(finding_target, finding, mobile_app, formatter, client)

    # And finding already has an 'ignored' comment in Jira
    ignore_message = JiraCommentTrackingTagEnum.TARGET_IGNORED.value
    ignore_message += " Some message here yo"
    with mock.patch(
        "dt_jira_integration.sync_with_jira_action.get_comments_from_issue",
        return_value=[ignore_message],
    ):
        jira_sync_action.perform()

    # Then DT still posts to Jira saying it's fixed
    reason = JiraCommentTrackingTagEnum.TARGET_FIXED.value
    msg = f"{reason} - Data Theorem detected that this issue has been addressed in the latest scan."

    client.add_comment.assert_called_once_with(client.get_issue(), msg)


@pytest.mark.parametrize("status", ["CLOSED_FIXED", "CLOSED_ITEM_NOT_FOUND"])
def test_finding_target_has_external_id_and_has_fix_messages_and_has_been_fixed(
    status: str, config: JiraIntegrationConfig, client: mock.Mock
) -> None:
    """This should not post anything since a comment has been posted saying issue is fixed"""
    # Given a finding with status of <status> that has been fixed
    finding = SecurityFinding(
        1,
        "title",
        "HIGH",
        "P1",
        "exploitability",
        "description",
        "recommendation",
        1,
        None,
        None,
        "portal_url",
        "HIGH",
        "code here",
    )
    finding_target = SecurityFindingTarget(
        1,
        "text",
        status,
        "date",
        2,
        "finding_id",
        finding.portal_url,
        None,
        None,
        external_id="external_id",
    )
    formatter = FindingTargetToJiraFormatter(config)
    mobile_app = mock.Mock()

    # When finding goes through the syncing process
    jira_sync_action = FindingTargetJiraSyncAction(finding_target, finding, mobile_app, formatter, client)

    # And finding already has a 'fixed' comment
    fix_message = JiraCommentTrackingTagEnum.TARGET_FIXED.value
    fix_message += " Some message here yo"
    with mock.patch(
        "dt_jira_integration.sync_with_jira_action.get_comments_from_issue",
        return_value=[fix_message],
    ):
        jira_sync_action.perform()

    # Then DT posts nothing
    assert client.add_comment.call_count == 0


@pytest.mark.parametrize(
    "status",
    ["CLOSED_COMPENSATING_CONTROL", "CLOSED_RISK_ACCEPTED", "OPEN_READY_TO_RESCAN"],
)
@pytest.mark.parametrize(
    "target_msg",
    [
        JiraCommentTrackingTagEnum.TARGET_FIXED.value,
        JiraCommentTrackingTagEnum.TARGET_IGNORED.value,
    ],
)
def test_finding_target_has_external_id_and_has_message_and_should_not_post_again(
    status: str,
    target_msg: str,
    config: JiraIntegrationConfig,
    client: mock.Mock,
) -> None:
    # Given a finding with status of <status>
    finding = SecurityFinding(
        1,
        "title",
        "HIGH",
        "P1",
        "exploitability",
        "description",
        "recommendation",
        1,
        None,
        None,
        "portal_url",
        "HIGH",
        "code here",
    )
    finding_target = SecurityFindingTarget(
        1,
        "text",
        status,
        "date",
        2,
        "finding_id",
        finding.portal_url,
        None,
        None,
        external_id="external_id",
    )
    formatter = FindingTargetToJiraFormatter(config)
    mobile_app = mock.Mock()

    # When finding goes through the syncing process
    jira_sync_action = FindingTargetJiraSyncAction(finding_target, finding, mobile_app, formatter, client)

    # And finding has a comment of type <target_ms>
    message = target_msg + " Some message here yo"
    with mock.patch(
        "dt_jira_integration.sync_with_jira_action.get_comments_from_issue",
        return_value=[message],
    ):
        jira_sync_action.perform()

    # Then DT posts nothing
    assert client.add_comment.call_count == 0


@pytest.mark.parametrize("status", ["CLOSED_RISK_ACCEPTED", "CLOSED_COMPENSATING_CONTROL"])
def test_finding_target_has_external_id_and_marked_as_closed_on_the_portal(
    status: str, config: JiraIntegrationConfig, client: mock.Mock
) -> None:
    # Given a finding with status of <status>
    finding = SecurityFinding(
        1,
        "title",
        "HIGH",
        "P1",
        "exploitability",
        "description",
        "recommendation",
        1,
        None,
        None,
        "portal_url",
        "HIGH",
        "code here",
    )
    finding_target = SecurityFindingTarget(
        1,
        "text",
        status,
        "date",
        2,
        "finding_id",
        finding.portal_url,
        None,
        None,
        external_id="external_id",
    )
    formatter = FindingTargetToJiraFormatter(config)
    mobile_app = mock.Mock()

    # When finding goes through the syncing process
    jira_sync_action = FindingTargetJiraSyncAction(finding_target, finding, mobile_app, formatter, client)

    # And finding has no comments
    with mock.patch(
        "dt_jira_integration.sync_with_jira_action.get_comments_from_issue",
        return_value=[],
    ):
        jira_sync_action.perform()

    # Then DT posts 'ignore' comment
    assert client.add_comment.call_count == 1

    closed_options = {
        "CLOSED_RISK_ACCEPTED": "Risk Accepted",
        "CLOSED_COMPENSATING_CONTROL": "Compensating Control",
    }
    msg = JiraCommentTrackingTagEnum.TARGET_IGNORED.value
    msg += (
        " - This issue has been manually closed as "
        f'"{closed_options[finding_target.current_status]}" in the Data Theorem portal.'
    )
    client.add_comment.assert_has_calls([call(client.get_issue(), msg)])


@pytest.mark.parametrize("status", ["OPEN", "NEW"])
@pytest.mark.parametrize(
    "fix_message",
    [
        JiraCommentTrackingTagEnum.TARGET_FIXED.value,
        JiraCommentTrackingTagEnum.TARGET_IGNORED.value,
    ],
)
def test_finding_target_has_external_id_and_has_message_and_is_reopened(
    fix_message: str,
    status: str,
    config: JiraIntegrationConfig,
    client: mock.Mock,
) -> None:
    """If a finding target has been reopened when it was previously closed in the DT portal and
    has been commented with a FIXED or IGNORED, we should post a comment saying it has been re-opened.
    This *only* applies if the status is OPEN.
    """
    # Given a finding target with a status of open which has been previously closed
    finding = SecurityFinding(
        1,
        "title",
        "HIGH",
        "P1",
        "exploitability",
        "description",
        "recommendation",
        1,
        None,
        None,
        "portal_url",
        "HIGH",
        "code here",
    )
    finding_target = SecurityFindingTarget(
        1,
        "text",
        status,
        "date",
        2,
        "finding_id",
        finding.portal_url,
        None,
        None,
        finding.portal_url,
        "external_id",
    )
    formatter = FindingTargetToJiraFormatter(config)
    mobile_app = mock.Mock()

    # And it already has a FIXED/IGNORED comment:
    with mock.patch(
        "dt_jira_integration.sync_with_jira_action.get_comments_from_issue",
        return_value=[fix_message],
    ):
        # When the finding goes through the syncing process
        jira_sync_action = FindingTargetJiraSyncAction(finding_target, finding, mobile_app, formatter, client)
        jira_sync_action.perform()
    # Then DT posts 're-open' comment
    assert client.add_comment.call_count == 1
    client.add_comment.assert_has_calls(
        [call(client.get_issue(), FindingTargetJiraSyncAction._COMMENT_TARGET_REOPENED)]
    )
