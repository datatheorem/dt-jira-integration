import pytest
from datatheorem_api_client import MobileApp
from datatheorem_api_client import SecurityFinding

from dt_jira_integration.utils import does_target_pass_export_filter
from dt_jira_integration.utils import does_target_pass_release_type_export

from .factories import MobileAppFactory
from .factories import SecurityFindingFactory


@pytest.mark.parametrize(
    "finding,export_filter,result",
    [
        (SecurityFindingFactory(), "DISABLED", False),
        (
            SecurityFindingFactory(priority="P0"),
            "ALL_ISSUES",
            True,
        ),
        (
            SecurityFindingFactory(priority="P1"),
            "ALL_ISSUES",
            True,
        ),
        (
            SecurityFindingFactory(priority="P1"),
            "P1_ISSUES_AND_BLOCKERS",
            True,
        ),
        (
            SecurityFindingFactory(priority="P3"),
            "P1_ISSUES_AND_BLOCKERS",
            False,
        ),
    ],
)
def test_export_filter(finding: SecurityFinding, export_filter: str, result: bool) -> None:
    # Given a finding and an export filter type
    # When we check if the finding should be processed
    # Then we see the result
    assert result == does_target_pass_export_filter(finding, export_filter)


@pytest.mark.parametrize(
    "mobile_app,allow_prod,allow_pre_prod,result",
    [
        (MobileAppFactory(release_type="APP_STORE"), True, False, True),
        (MobileAppFactory(release_type="APP_STORE"), False, True, False),
        (MobileAppFactory(release_type="PRE_PROD"), True, False, False),
        (MobileAppFactory(release_type="PRE_PROD"), False, True, True),
    ],
)
def test_release_type_export(mobile_app: MobileApp, allow_prod: bool, allow_pre_prod: bool, result: bool) -> None:
    # Give an mobile app, and some boolean rules
    # When we check if it passes the release type check
    # Then we see the result
    assert result == does_target_pass_release_type_export(mobile_app, allow_prod, allow_pre_prod)
