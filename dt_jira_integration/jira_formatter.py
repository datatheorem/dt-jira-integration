from abc import ABCMeta
from abc import abstractmethod
from typing import Any
from typing import Dict
from typing import List
from typing import Optional

from datatheorem_api_client import JiraIntegrationConfig
from datatheorem_api_client import SecurityFinding
from datatheorem_api_client import SecurityFindingTarget
from datatheorem_api_client.results_api.jira_integration_config import JiraField

from dt_jira_integration.utils import get_severity_level


class Formatter(metaclass=ABCMeta):
    @abstractmethod
    def get_create_issue_dict(
        self,
        finding_target: SecurityFindingTarget,
        finding: SecurityFinding,
        mobile_app_name: str,
        mobile_platform: str,  # Enum,
    ) -> Dict[str, Any]:
        pass


class FindingTargetToJiraFormatter(Formatter):
    """This implements the formatting logic for exporting a DT target to a Jira ticket."""

    def __init__(self, integration_config: JiraIntegrationConfig) -> None:
        # For formatting, we only need to keep the following
        self._project_id = integration_config.project_key_or_id
        self._type_issue_name = integration_config.type_of_issue_name
        self._severity_field = integration_config.severity_field_config
        self._constant_custom_fields = integration_config.static_fields
        self._dynamic_custom_fields = integration_config.dynamic_fields

    def get_create_issue_dict(
        self,
        finding_target: SecurityFindingTarget,
        finding: SecurityFinding,
        mobile_app_name: str,
        mobile_platform: str,  # PlatformEnum,
        internal_app_id: Optional[str] = None,
    ) -> Dict[str, Any]:
        """Return a dictionary with all the arguments ready to be passed to JIRA.create_issue()."""
        # First generate the content of the ticket
        issue_dict: Dict[str, Any] = {
            "project": {"key": self._project_id},  # self._project_id,
            "issuetype": {"name": self._type_issue_name},  # self._type_issue_name,
            "summary": f"[{mobile_platform}] {mobile_app_name}: {finding.title}",
        }

        description = ""
        if internal_app_id:
            description = f"Internal App ID: {internal_app_id}\n"

        description += (
            f"URL: {finding_target.portal_url}\n\n"
            f"h3. Affected Component\n{finding_target.text}\n"
            f"h3. Description\n{finding.description}\n"
            f"h3. Recommendation\n{finding.recommendation}\n\n"
        )

        if finding.secure_code:
            description += f"h3. Secure Code\n{finding.secure_code}\n"

        # Since we have enforced that all code blocks use 3 back-ticks (```),
        # it's safe to assume that we can replace all instances of those with Jira's version of code blocks
        description = description.replace("```", "{code}")

        issue_dict["description"] = description

        # Then, add the custom fields
        for field in self._constant_custom_fields:
            # TODO(Gopar): Check here for diff ways of saving values
            # When the value is pure digits, we assume it's associate to a specific value on Jira's end so we send
            # it in a dict with the id. Otherwise we assume it's just plain text
            value = field.field_value if not field.field_value.isdigit() else {"id": field.field_value}
            issue_dict[field.field_id] = value

        for field in self._dynamic_custom_fields:
            if field.value == "FINDING_PORTAL_URL":
                issue_dict[field.field_id] = finding_target.portal_url

        # Add the severity field
        if self._severity_field:
            issue_dict[self._severity_field.field_id] = {
                "id": get_severity_level(self._severity_field, finding.severity)
            }

        return issue_dict


class TargetDTFormatter(Formatter):
    """Implements the new formatting logic going forward for exporting a DT target to a jira ticket.
    No longer uses static/dynamic fields. Only Jira Fields
    """

    def __init__(self, integration_config: JiraIntegrationConfig) -> None:
        self._project_key = integration_config.project_key_or_id
        self._issue_type_name = integration_config.type_of_issue_name
        self._severity_field = integration_config.severity_field_config
        self._jira_fields = integration_config.jira_fields

    def get_create_issue_dict(
        self,
        target: SecurityFindingTarget,
        finding: SecurityFinding,
        mobile_app_name: str,
        mobile_platform: str,  # PlatformEnum,
        internal_app_id: Optional[str] = None,
    ) -> Dict[str, Any]:
        """Return a dictionary that `JIRA.create_issue()` accepts"""
        issue_dict = {
            "project": {"key": self._project_key},
            "issuetype": {"name": self._issue_type_name},
            "summary": f"[{mobile_platform}] {mobile_app_name}: {finding.title}",
        }

        description = ""
        if internal_app_id:
            description = f"Internal App ID: {internal_app_id}\n"

        description += (
            f"URL: {target.portal_url}\n\n"
            f"h3. Affected Component\n{target.text}\n"
            f"h3. Description\n{finding.description}\n"
            f"h3. Recommendation\n{finding.recommendation}\n\n"
        )

        if finding.secure_code:
            description += f"h3. Secure Code\n{finding.secure_code}\n"

        # Since we have enforced that all code blocks use 3 back-ticks (```),
        # it's safe to assume that we can replace all instances of those with Jira's version of code blocks
        description = description.replace("```", "{code}")
        issue_dict["description"] = description

        # Add the severity field
        if self._severity_field:
            issue_dict[self._severity_field.field_id] = {
                "id": get_severity_level(self._severity_field, finding.severity)
            }

        # Add jira fields
        customfields_payload = self.map_fields(self._jira_fields)
        issue_dict.update(customfields_payload)

        return issue_dict

    def map_fields(self, fields: List[JiraField]) -> Dict[str, Any]:
        """Figure out how to build a payload for each field that Jira will accept."""
        payloads = dict()
        helper: Any
        for field in fields:
            if field.type == "STRING":
                payloads[field.field_id] = field.string_value

            elif field.type == "STRING_ARRAY":
                payloads[field.field_id] = field.string_array_value

            elif field.type == "NUMBER":
                payloads[field.field_id] = field.number_value

            elif field.type == "OPTION":
                helper = dict(value=field.option_value)
                payloads[field.field_id] = helper

            elif field.type == "COMPONENT_ARRAY":
                helper = [dict(name=component) for component in field.component_array_value]
                payloads[field.field_id] = helper

            elif field.type == "OPTION_ARRAY":
                helper = [dict(value=option) for option in field.option_array_value]
                payloads[field.field_id] = helper

            elif field.type == "USER":
                payloads[field.field_id] = dict(accountId=field.user_value)

        return payloads
