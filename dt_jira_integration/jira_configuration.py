import yaml
from datatheorem_api_client import JiraIntegrationConfig


GLOBAL_JIRA_INTEGRATION_ID = 9


def jira_config_from_yaml(yaml_path: str) -> JiraIntegrationConfig:
    with open(yaml_path, "r") as yaml_file:
        # When we are loading from a yaml file, we will assume it is the global config
        jira_config = yaml.safe_load(yaml_file)

    jira_config["id"] = GLOBAL_JIRA_INTEGRATION_ID
    jira_config["is_global"] = True

    return JiraIntegrationConfig.from_dict(jira_config)
