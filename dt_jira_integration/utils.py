from enum import Enum
from typing import List
from urllib.parse import urljoin

import requests
from datatheorem_api_client import JiraIntegrationConfig
from datatheorem_api_client import MobileApp
from datatheorem_api_client import ResultsAPIClient
from datatheorem_api_client import SecurityFinding
from datatheorem_api_client import SecurityFindingTarget
from datatheorem_api_client.results_api.jira_integration_config import SeverityFieldConfig
from requests.auth import HTTPBasicAuth

from dt_jira_integration.logger import jira_logger


class JiraCommentTrackingTagEnum(Enum):
    """A tag we set on a Jira ticket by adding a comment with it, in order to keep track of the sync state."""

    # The tag for flagging the fact that a specific target was fixed in the DT portal
    TARGET_FIXED = "DT-TARGET-FIXED"

    # The tag for flagging the fact that a target was closed as Risk Accepted or Compensating Control in the DT portal
    TARGET_IGNORED = "DT-TARGET-IGNORED"

    # The tag for flagging the fact that a target was re-opened
    TARGET_REOPENED = "DT-TARGET-REOPENED"


# The following 2 functions are needed because we are unable to fethc comments via the 3rd party lib jira
# See issue: https://github.com/pycontribs/jira/issues/486
def get_comments_from_issue(jira_config: JiraIntegrationConfig, issue_id_or_key: str) -> List[str]:
    """Retrive all the comments from issue. Returns only the body of comment"""
    url = urljoin(jira_config.base_url, "/rest/api/2/issue/{}/comment".format(issue_id_or_key))
    response = requests.get(url, auth=HTTPBasicAuth(jira_config.username, jira_config.password))

    jira_logger.info(
        f"HTTP response for GET {url}:\n" f" - Status: {response.status_code}\n" f" - Content: {response.content!r}"
    )
    response.raise_for_status()

    comments = [comment["body"] for comment in response.json()["comments"]]
    return comments


def does_have_dt_comment(comments: List[str], comment_tag: JiraCommentTrackingTagEnum) -> bool:
    """Returns True if comments contains `comment_tag`"""
    for comment in comments:
        if comment_tag.value in comment:
            return True
    return False


def does_target_pass_export_filter(finding: SecurityFinding, export_filter: str) -> bool:
    """Returns True if target is supposed to be processed.

    export_filter decides whether it should be processed or not.
    Currently the options that customer has is P1+Blockers, All Issues, or Disabled
    """
    # Customer has stated that they want everything exported
    if export_filter == "ALL_ISSUES":
        return True

    # Only allow P1 stuff to export
    if export_filter == "P1_ISSUES_AND_BLOCKERS" and finding.priority == "P1":
        return True

    # Also valid if it's a Google/App Store Blocker
    SL1 = set(finding.importance_tags)
    SL2 = set(["GOOGLE_P1", "APPLE_P1"])
    if export_filter == "P1_ISSUES_AND_BLOCKERS" and SL1.intersection(SL2):
        return True

    return False


def does_target_pass_release_type_export(mobile_app: MobileApp, allow_prod: bool, allow_pre_prod: bool) -> bool:
    """Returns True if the targets release type matches what the customer wants exported

    Current options are Prod, Pre-Prod or Both (Meaning Prod and Pre-prod are both True)
    """
    if mobile_app.release_type == "APP_STORE" and allow_prod:
        return True
    if mobile_app.release_type == "PRE_PROD" and allow_pre_prod:
        return True
    return False


def all_mobile_apps(results_client: ResultsAPIClient) -> List[MobileApp]:
    """Fetch all mobile apps within a customer"""
    all_mobile_apps: List[MobileApp] = []
    all_mobile_apps, next_cursor = results_client.list_mobile_apps()

    while next_cursor:
        new_apps, next_cursor = results_client.list_mobile_apps(cursor=next_cursor)
        all_mobile_apps += new_apps

    return all_mobile_apps


def all_security_finding_targets(
    results_client: ResultsAPIClient,
) -> List[SecurityFindingTarget]:
    """Fetch all security finding targets within a customer"""
    all_security_finding_targets: List[SecurityFindingTarget]
    (
        all_security_finding_targets,
        next_cursor,
    ) = results_client.list_security_finding_targets()

    while next_cursor:
        new_targets, next_cursor = results_client.list_security_finding_targets(cursor=next_cursor)
        all_security_finding_targets += new_targets

    return all_security_finding_targets


def all_security_findings(results_client: ResultsAPIClient) -> List[SecurityFinding]:
    """Fetch all security findings within a customer"""
    all_security_findings: List[SecurityFinding]
    all_security_findings, next_cursor = results_client.list_security_findings()

    while next_cursor:
        new_findings, next_cursor = results_client.list_security_findings(cursor=next_cursor)
        all_security_findings += new_findings

    return all_security_findings


def all_jira_integration_configs(
    results_client: ResultsAPIClient,
) -> List[JiraIntegrationConfig]:
    """Fetch all jira integration configs withi na customer"""
    # Make a function here in case we implement pagination
    return results_client.list_jira_integration_configs()


def is_config_valid(
    jira_config: JiraIntegrationConfig,
    security_finding: SecurityFinding,
    mobile_app: MobileApp,
) -> bool:
    """Check if we should work with this config.
    Customer might of set certain settings that restricts what configs we should work with.
    """
    # If the customer has the Jira integration turned off, then don't do a sync
    if jira_config.export_filter == "DISABLED":
        jira_logger.info("Customer has jira integration disabled for config ID of {}".format(jira_config.id))
        return False

    # Customer might of specified which type of issues we should export (P1 only, all issues, Disabled)
    if not does_target_pass_export_filter(security_finding, jira_config.export_filter):
        return False

    # Customer might of specified which release issues we should only export (Pre-Prod, Prod, or both)
    if not does_target_pass_release_type_export(mobile_app, jira_config.export_prod, jira_config.export_pre_prod):
        return False

    # Looks like we can work with this config
    return True


def get_severity_level(severity_field_config: SeverityFieldConfig, level: str) -> str:  # SeverityEnum
    """Return the severity value ID based on the level."""
    options = {
        "LOW": severity_field_config.low_severity_value_id,
        "MEDIUM": severity_field_config.medium_severity_value_id,
        "HIGH": severity_field_config.high_severity_value_id,
    }

    return options[level]
